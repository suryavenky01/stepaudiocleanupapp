﻿using Rollbar;
using System;
using System.IO;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace StepAudioCleanUp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
               IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

               RollbarLocator.RollbarInstance
                    .Configure(new RollbarConfig(config["RollbarSettings:RollbarAccessToken"]) { Environment = config["RollbarSettings:RollbarEnvironment"] });
            
                RollbarLocator.RollbarInstance.Info("Audio cleanUp job started");

                //1. Env set
                string environment = config["AudioFolderLocation"];
                var files = new DirectoryInfo(@environment).GetDirectories();

                //2. Check if folder exist or not
                if (files.Length > 0)
                {
                    foreach (DirectoryInfo dir in files)
                    {
                        if ((DateTime.UtcNow - dir.CreationTimeUtc) > TimeSpan.FromDays(1))
                        {
                            dir.Delete(true);
                        }
                    }
                }
                else
                {
                    RollbarLocator.RollbarInstance.Error("No audio folder found to delete");
                }
                RollbarLocator.RollbarInstance.Info("Audio cleanUp job ended");
            }
            catch (Exception ex)
            {
                RollbarLocator.RollbarInstance.Critical(ex);
            }
        }

        /// <summary>
        /// Configures the Rollbar singleton-like notifier.
        /// </summary>
        private static void ConfigureRollbarSingleton()
        {
            const string rollbarAccessToken = "7e5b46c42a7046889732d376c96f2772";
            const string rollbarEnvironment = "dev";

            var config = new RollbarConfig(rollbarAccessToken) // minimally required Rollbar configuration
            {
                Environment = rollbarEnvironment,
                ScrubFields = new string[]
                {
                    "access_token", // normally, you do not want scrub this specific field (it is operationally critical), but it just proves safety net built into the notifier... 
                    "username",
                }
            };
            RollbarLocator.RollbarInstance
                // minimally required Rollbar configuration:
                .Configure(config)
                // optional step if you would like to monitor this Rollbar instance's internal events within your application:
                .InternalEvent += OnRollbarInternalEvent
                ;

            // optional step if you would like to monitor all Rollbar instances' internal events within your application:
            RollbarQueueController.Instance.InternalEvent += OnRollbarInternalEvent;

            // Optional info about reporting Rollbar user:
            SetRollbarReportingUser("Console App - STEP Server");
        }

        /// <summary>
        /// Sets the rollbar reporting user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="email">The email.</param>
        /// <param name="userName">Name of the user.</param>
        private static void SetRollbarReportingUser(string id)
        {
            Rollbar.DTOs.Person person = new Rollbar.DTOs.Person(id);
            //person.Email = email;
            //person.UserName = userName;
            RollbarLocator.RollbarInstance.Config.Person = person;
        }

        /// <summary>
        /// Called when rollbar internal event is detected.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RollbarEventArgs"/> instance containing the event data.</param>
        private static void OnRollbarInternalEvent(object sender, RollbarEventArgs e)
        {
            Console.WriteLine(e.TraceAsString());

            RollbarApiErrorEventArgs apiErrorEvent = e as RollbarApiErrorEventArgs;
            if (apiErrorEvent != null)
            {
                //TODO: handle/report Rollbar API communication error event...
                return;
            }
            CommunicationEventArgs commEvent = e as CommunicationEventArgs;
            if (commEvent != null)
            {
                //TODO: handle/report Rollbar API communication event...
                return;
            }
            CommunicationErrorEventArgs commErrorEvent = e as CommunicationErrorEventArgs;
            if (commErrorEvent != null)
            {
                //TODO: handle/report basic communication error while attempting to reach Rollbar API service... 
                return;
            }
            InternalErrorEventArgs internalErrorEvent = e as InternalErrorEventArgs;
            if (internalErrorEvent != null)
            {
                //TODO: handle/report basic internal error while using the Rollbar Notifier... 
                return;
            }
        }
    }
}
